# Load the Rails application.
require File.expand_path('../application', __FILE__)

#Paperclip.options[:command_path] = "/opt/local/bin/"
Paperclip.options[:command_path] = "/usr/local/Cellar/imagemagick"

# Initialize the Rails application.
Rails.application.initialize!
