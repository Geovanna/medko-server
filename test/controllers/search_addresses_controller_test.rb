require 'test_helper'

class SearchAddressesControllerTest < ActionController::TestCase
  setup do
    @search_address = search_addresses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:search_addresses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create search_address" do
    assert_difference('SearchAddress.count') do
      post :create, search_address: { address: @search_address.address, complement: @search_address.complement, latitude: @search_address.latitude, longitude: @search_address.longitude, user_id: @search_address.user_id }
    end

    assert_redirected_to search_address_path(assigns(:search_address))
  end

  test "should show search_address" do
    get :show, id: @search_address
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @search_address
    assert_response :success
  end

  test "should update search_address" do
    patch :update, id: @search_address, search_address: { address: @search_address.address, complement: @search_address.complement, latitude: @search_address.latitude, longitude: @search_address.longitude, user_id: @search_address.user_id }
    assert_redirected_to search_address_path(assigns(:search_address))
  end

  test "should destroy search_address" do
    assert_difference('SearchAddress.count', -1) do
      delete :destroy, id: @search_address
    end

    assert_redirected_to search_addresses_path
  end
end
