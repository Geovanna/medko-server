require 'test_helper'

class DeviceIdsControllerTest < ActionController::TestCase
  setup do
    @device_id = device_ids(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:device_ids)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create device_id" do
    assert_difference('DeviceId.count') do
      post :create, device_id: { os: @device_id.os, token: @device_id.token, user_id: @device_id.user_id }
    end

    assert_redirected_to device_id_path(assigns(:device_id))
  end

  test "should show device_id" do
    get :show, id: @device_id
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @device_id
    assert_response :success
  end

  test "should update device_id" do
    patch :update, id: @device_id, device_id: { os: @device_id.os, token: @device_id.token, user_id: @device_id.user_id }
    assert_redirected_to device_id_path(assigns(:device_id))
  end

  test "should destroy device_id" do
    assert_difference('DeviceId.count', -1) do
      delete :destroy, id: @device_id
    end

    assert_redirected_to device_ids_path
  end
end
