class CreateDeviceIds < ActiveRecord::Migration
  def change
    create_table :device_ids do |t|
      t.string :token
      t.string :os
      t.string :session_id
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
