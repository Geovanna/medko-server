class CreateServiceWindows < ActiveRecord::Migration
  def change
    create_table :service_windows do |t|
      t.references :user, index: true, foreign_key: true
      t.integer :week_day
      t.time :start_time
      t.time :end_time
      t.integer :service_time
      t.float :value
      t.references :place, index: true, foreign_key: true
      t.references :attendence,foreign_key: true, index: true
      t.references :exam_type, index: true, foreign_key: true
      t.references :speciality, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
