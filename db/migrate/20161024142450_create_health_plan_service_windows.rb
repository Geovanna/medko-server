class CreateHealthPlanServiceWindows < ActiveRecord::Migration
  def change
    create_table :health_plans_service_windows do |t|
      t.references :service_window, index: true, foreign_key: true
      t.references :health_plan, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
