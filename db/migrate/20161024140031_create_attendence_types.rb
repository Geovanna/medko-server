class CreateAttendenceTypes < ActiveRecord::Migration
  def change
    create_table :attendence_types do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
