class CreateExamTypes < ActiveRecord::Migration
  def change
    create_table :exam_types do |t|
      t.string :name
      t.boolean :home_care

      t.timestamps null: false
    end
  end
end
