class AddAttachmentCrmImageToUsers < ActiveRecord::Migration
  def self.up
    change_table :users do |t|
      t.attachment :crm_image
    end
  end

  def self.down
    remove_attachment :users, :crm_image
  end
end
