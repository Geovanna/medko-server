class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :password
      t.string :telephone
      t.string :crm
      t.string :full_name
      t.string :zip_code
      t.string :address
      t.boolean :active,:default => true
      t.boolean :blocked
      t.boolean :facebook_account
      t.boolean :rating
      t.string :facebook_token
      t.string :blocked_message
      t.float :latitude
      t.float :longitude
      t.float :value_home_care
      t.datetime :date_location
      t.references :account_type, index: true, foreign_key: true,:default => 1
      t.timestamps null: false
    end
  end
end
