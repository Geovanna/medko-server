json.array!(@attendences) do |attendence|
  json.extract! attendence,:id, :date,:value

 	 json.url attendence_url(attendence, format: :json)

	if attendence.speciality_id != nil 
  	    json.speciality do |json|
			json.id attendence.speciality.id
			json.name attendence.speciality.name
		end
	end	

	if attendence.exam_type_id != nil 
		json.exam_type do |json|
			json.id attendence.exam_type.id
			json.name attendence.exam_type.name
		end
	end	
		
		json.attendence_status do |json|
			json.id attendence.attendence_status.id
			json.name attendence.attendence_status.name
		end

		json.attendence_type do |json|
			json.id attendence.attendence_type.id
			json.name attendence.attendence_type.name
		end

		json.patient do |json|
			json.id attendence.patient.id
			json.name attendence.patient.name
			json.avatar attendence.patient.avatar.url
			json.email attendence.patient.email
			json.telephone attendence.patient.telephone
			json.latitude attendence.patient.latitude
			json.longitude attendence.patient.longitude
		end

	if attendence.payment_method_id != nil 
		json.payment_method do |json|
			json.id  attendence.payment_method.id
			json.name  attendence.payment_method.name
		end
	end	

		json.doctor do |json|
			json.id attendence.doctor.id
			json.name attendence.doctor.name
			json.avatar attendence.doctor.avatar.url
			json.telephone attendence.doctor.telephone
			json.crm attendence.doctor.crm
			json.media attendence.doctor.media
			json.rating attendence.doctor.rating
			json.email attendence.doctor.email
			json.latitude attendence.doctor.latitude
			json.longitude attendence.doctor.longitude
		end

	if attendence.health_plan_id != nil 
		json.health_plan do |json|
			json.id attendence.health_plan.id
			json.name attendence.health_plan.name
		end
	end 

	if attendence.place_id != nil 
		json.place do |json|
			json.id attendence.place.id
			json.name attendence.place.name
			json.address attendence.place.address
			json.complement attendence.place.complement
			json.latitude attendence.place.latitude
			json.longitude attendence.place.longitude
		end
	end 


	if attendence.payment_id != nil 
		json.payment do |json|
			json.id attendence.payment.id
			json.price attendence.payment.price
		end
	end 

	if attendence.classification_id != nil 
		json.classification do |json|
			json.id attendence.classification.id
			json.value attendence.classification.value
		end
	end 

     if attendence.service_window_id != nil 
		json.service_window do |json|
			json.id attendence.service_window.id
			json.week_day attendence.service_window.week_day
		    json.start_time attendence.service_window.start_time
		    json.end_time attendence.service_window.end_time
		    json.service_time attendence.service_window.service_time
		end
	end

 	if attendence.search_address_id != nil 
		json.search_address do |json|
			json.address attendence.search_address.address
		    json.complement attendence.search_address.complement
		    json.latitude attendence.search_address.latitude
		    json.longitude attendence.search_address.longitude
			json.id attendence.search_address.id
	end
		end

end
