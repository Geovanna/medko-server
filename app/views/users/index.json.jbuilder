json.array!(@users) do |user|
  json.extract! user, :id, :avatar,:value_home_care,:address,:zip_code, :name, :email, :password, :telephone, :crm, :rating, :crm_image, :proof_of_address_image,:full_name, :active, :blocked, :blocked_message, :account_type_id, :media
  json.url user_url(user, format: :json)
end


