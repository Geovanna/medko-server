json.array!(@exam_types) do |exam_type|
  json.extract! exam_type, :id, :name,:home_care,:image_url
  json.url exam_type_url(exam_type, format: :json)
end
