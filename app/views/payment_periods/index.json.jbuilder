json.array!(@payment_periods) do |payment_period|
  json.extract! payment_period, :id, :start_date, :end_date, :accomplished
  json.url payment_period_url(payment_period, format: :json)
end
