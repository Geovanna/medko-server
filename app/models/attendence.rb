require 'fcm'
require 'geokit'
class Attendence < ActiveRecord::Base
#after_save :do_notification

  belongs_to :speciality
  belongs_to :exam_type
  belongs_to :place
  belongs_to :search_address
  belongs_to :service_window
  belongs_to :payment_method
  belongs_to :attendence_type
  belongs_to :attendence_status
  belongs_to :payment
  belongs_to :health_plan
  belongs_to :attendence_type
  has_one :classification
  belongs_to :patient, :foreign_key => "patient_id", :class_name => "User"
  belongs_to :doctor,  :foreign_key => "doctor_id", :class_name => "User"


  accepts_nested_attributes_for :classification
  accepts_nested_attributes_for :patient
  accepts_nested_attributes_for :doctor  
  accepts_nested_attributes_for :exam_type
  accepts_nested_attributes_for :speciality
  accepts_nested_attributes_for :search_address
  accepts_nested_attributes_for :payment_method
  accepts_nested_attributes_for :service_window
  accepts_nested_attributes_for :attendence_type
  accepts_nested_attributes_for :attendence_status
  accepts_nested_attributes_for :payment
  accepts_nested_attributes_for :place

    validates :date,:presence => false, :uniqueness => {:scope => :service_window,:message => "^Horario já ocupado"}, :allow_blank => true, :allow_nil => true

    
    def self.search(query)
     joins(
     "LEFT JOIN places ON attendences.place_id = places.id 
     LEFT JOIN attendence_statuses ON attendences.attendence_status_id = attendence_statuses.id 
     LEFT JOIN health_plans ON attendences.health_plan_id = health_plans.id
     LEFT JOIN payments ON attendences.payment_id = payments.id
     LEFT JOIN users ON attendences.patient_id = users.id or attendences.doctor_id = users.id
     LEFT JOIN specialities ON attendences.speciality_id = specialities.id
     LEFT JOIN exam_types ON attendences.exam_type_id = exam_types.id").where("specialities.name like? OR exam_types.name like? OR cast(date as text) like? OR attendence_statuses.name like ? OR health_plans.name like ? OR cast(payments.price as text) like ? OR users.name like ? OR places.name like ?","%#{query}%","%#{query}%","%#{query}%","%#{query}%","%#{query}%","%#{query}%","%#{query}%","%#{query}%").limit(10).order("created_at DESC").select('distinct attendences.*')
    end

    def self.search_between(query,inicio,fim)
       where(date: inicio..fim).limit(50).order("date DESC")
    end

   def self.search_clinic(exam_type_id,payment_id,speciality_id,health_plan_id,latitude,longitude,patient_id)
    if health_plan_id != "" && exam_type_id != ""
      service_window = ServiceWindow.joins("JOIN health_plans_service_windows on service_windows.id = health_plans_service_windows.service_window_id JOIN places ON service_windows.place_id = places.id").where("cast(exam_type_id as text) like? AND cast(health_plans_service_windows.health_plan_id as text) like?","%#{exam_type_id}%","%#{health_plan_id}%").by_distance(:origin => [latitude,longitude]).order(start_time:  :asc, value: :desc)
    elsif health_plan_id != "" && speciality_id != ""
      service_window = ServiceWindow.joins("JOIN health_plans_service_windows on service_windows.id = health_plans_service_windows.service_window_id JOIN places ON service_windows.place_id = places.id").where("cast(speciality_id as text) like? AND cast(health_plans_service_windows.health_plan_id as text) like?","%#{speciality_id}%","%#{health_plan_id}%").by_distance(:origin => [latitude,longitude]).order(start_time:  :asc, value: :desc)
    elsif payment_id != "" && exam_type_id != ""
       service_window = ServiceWindow.joins("JOIN payment_methods_service_windows on service_windows.id = payment_methods_service_windows.service_window_id JOIN places ON service_windows.place_id = places.id").where("cast(exam_type_id as text) like? AND cast(payment_methods_service_windows.payment_method_id as text) like?","%#{exam_type_id}%","%#{payment_id}%").by_distance(:origin => [latitude,longitude]).order(start_time:  :asc, value: :desc)
    elsif payment_id != "" && speciality_id != ""
        service_window = ServiceWindow.joins("JOIN payment_methods_service_windows on service_windows.id = payment_methods_service_windows.service_window_id JOIN places ON service_windows.place_id = places.id").where("cast(speciality_id as text) like? AND cast(payment_methods_service_windows.payment_method_id as text) like?","%#{speciality_id}%","%#{payment_id}%").by_distance(:origin => [latitude,longitude]).order(start_time:  :asc, value: :desc)
    end
       return service_window
   end

  def self.search_home_care(address,latitude,longitude,exam_type_id,speciality_id,patient_id)
    search_address = SearchAddress.new
    search_address.latitude = latitude
    search_address.longitude = longitude
    search_address.address = address
    search_address.user_id = patient_id
    search_address.save
    attendence = Attendence.new

    if exam_type_id != ""
     doctors = User.joins("JOIN exam_types_users on users.id = exam_types_users.user_id").where(["exam_types_users.exam_type_id = ?", exam_type_id]).closest(:units => :kms, :origin => [latitude,longitude])
    elsif speciality_id != ""
      doctors = User.joins("JOIN specialities_users on users.id = specialities_users.user_id").where(["specialities_users.speciality_id = ?", speciality_id]).closest(:units => :kms, :origin => [latitude,longitude])
    end

    doctors.each do |doctor| 

      if doctor.date_location + 10*60 > Time.now
        attendence.exam_type_id = exam_type_id
        attendence.speciality_id = speciality_id
        attendence.attendence_status_id = 7
        attendence.attendence_type_id = 1
        attendence.doctor_id = doctor.id
        attendence.patient_id = patient_id
        attendence.search_address_id = search_address.id
        attendence.value = doctor.value_home_care
        attendence.save
        return attendence
      end
    end 
  end   

  def payment_request
    payment = PagSeguro::PaymentRequest.new
    payment.abandon_url = "http://localhost:3000/attendences/" << id.to_s
    payment.notification_url = "http://localhost:3000/pagseguro/notifications"
    payment.redirect_url = "http://localhost:3000/attendences/" << id.to_s

    payment.credentials = PagSeguro::AccountCredentials.new('augustuscosta@gmail.com', '2611659A82DE4AAC98F5C5F26E93F42C')
    
    payment.items << {
      id: id,
      description: %[Pagamento referente a consulta marcada pelo site Medko],
      amount: doctor.try(:value_home_care)
    }

    payment.reference = id

    logger.info "=> REQUEST"
    logger.info PagSeguro::PaymentRequest::RequestSerializer.new(payment).to_params

    response = payment.register

    logger.info ""
    logger.info "=> RESPONSE"
    logger.info response.code
    logger.info response.created_at
    logger.info response.errors.to_a

    if response.errors.any?
      raise response.errors.join("\n")
    else
    return response.url #URL PARA EFETUAR O PAGAMENTO
  end
end  
    
  private

    def do_notification
      if attendence_status == nil
        return
      end

      UserMailer.attendence_status_change_mail(doctor, attendence_status.doctor_message, self).deliver_now
      UserMailer.attendence_status_change_mail(patient, attendence_status.patient_message, self).deliver_now

      notify_attendence_status_changed_fcm(get_registration_ids(doctor), attendence_status.doctor_message)
      notify_attendence_status_changed_fcm(get_registration_ids(patient),attendence_status.patient_message)
    end

    def get_registration_ids(user)
      registration_ids = Array.new
      for device_id in user.device_id do
        registration_ids.push(device_id.token)
      end
      return registration_ids
    end

    def notify_attendence_status_changed_fcm(registration_ids, message)
      if registration_ids.size < 1
        return
      end

      fcm = FCM.new("AIzaSyAGhqn84UhukXejVsQE2KhQTl7MlcaFdGU")
      options = {data: {notification_type: 'ATTENDENCE_STATUS_CHANGE', attendence_id: id, message: message}, collapse_key: "updated_attendence", priority: "high"}
      response = fcm.send(registration_ids, options)

    end
end


