class HomeCareService < ActiveRecord::Base
  belongs_to :exam_type
  belongs_to :speciality
  belongs_to :user
  has_many :attendence
end
