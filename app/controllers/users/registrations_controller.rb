class Users::RegistrationsController < Devise::RegistrationsController

 skip_before_action :require_no_authentication, only: [:new, :create]


	   def new
        @bank_account = nil 
        build_resource({})
        resource.bank_account.build
        respond_with self.resource
      end

      def paciente
      @user = User.new
      @user.save
      end

      def adm
       @user = User.new
      end

      def medico
      @user = User.new
      @exam_types = ExamType.all
      @specialities = Speciality.all
      end

      def update_resource(resource, params)
        resource.update_without_password(params)
      end


    def create
      super 
    end

  protected

    def sign_up(resource_name, resource)
       if current_user == nil 
        sign_in(resource_name, resource)
      end
    end

    def after_sign_up_path_for(resource)
      if current_user.account_type_id != 2
        root_path
      else  
        users_path
      end   
    end


    

private

  def sign_up_params
    params.require(:user).permit(:name,:address,:zip_code, :rating, :value_home_care, :active, :avatar,:email, :password,:telephone,:crm,:crm_image, :proof_of_address_image,{:exam_type_ids => []},{:speciality_ids => []},:bank, :full_name,:blocked, :blocked_message, :account_type_id,bank_account_attributes:[:bank, :full_name, :cpf_cnpj,:account,:operation,:user,:agency])
  end
end