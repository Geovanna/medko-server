class Api::UsersController < Api::BaseController
  before_filter :authenticate_user!

  respond_to :json
 

  def current_user_session
    @user = current_user
    render 'users/show.json.jbuilder'
  end

end