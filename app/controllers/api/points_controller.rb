class Api::PointsController < Api::BaseController
  before_filter :authenticate_user!

  respond_to :json
 

def location
 	current_user.latitude = params['latitude']
	current_user.longitude = params['longitude']
	current_user.date_location = DateTime.now
	current_user.save
    render json: {nothing: true}
end
	
end
